import collectionsList from './collectionsList.pug';
import collectionPhotos from './collectionPhotos.pug';
import loading from './loading.pug';
export default {
  collectionsList,
  collectionPhotos,
  loading
}
