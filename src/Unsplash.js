class Unsplash {
  constructor({ clientId } = { clientId: null }) {
    this.clientId = clientId;
    this.reqHeader = {
      Authorization:
          `Client-ID ${clientId}`,
    };
  }

  getCollections = ({ page: page = 1, perPage: perPage = 10 }) => {
    const list = fetch(`https://api.unsplash.com/collections?page=${page}&per_page=${perPage}`, {
      method: 'GET',
      headers: this.reqHeader,
    }).then(res => res.json())
      .then(data => ({ list: data })).catch(error => error);
    return list;
  }

  getCollectionPhotos = ({ id: id = null }) => {
    if (id) {
      const photos = fetch(`https://api.unsplash.com/collections/${id}/photos?per_page=16`, {
        method: 'GET',
        headers: this.reqHeader,
      }).then(res => res.json())
        .then(data => ({ photos: data })).catch(error => error);
      return photos;
    }
    return ({error: 'no collection id'})
  }
}

export default Unsplash;
