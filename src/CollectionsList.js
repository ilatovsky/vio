import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';

class CollectionsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: props.page,
      perPage: props.perPage,
      client: props.client,
    };
  }

  fetchCollections = () => {
    const { client, page, perPage } = this.props;
    client.getCollectionsList({ page, perPage }).then((data) => {
      this.setState({ collections: data.map(item => ({ title: item.title })) });
    });
  }

  static getDerivedStateFromProps(props, state) {
    const newState = {};
    if (props.page !== state.page) newState.page = props.page;
    if (props.perPage !== state.perPage) newState.perPage = props.perPage;
    return newState;
  }

  componentDidUpdate() {
    this.fetchCollections();
  }

  componentDidMount() {
    this.fetchCollections();
  }

  render() {
    const { collections } = this.state;
    return (
      <Grid container spacing={8} alignItems='stretch'>
        {(collections)
          ? collections.map((collection, i) => (
            <Grid key={i} item xs={3}>
              <Card onClick={() => console.log(collection.title)}>
                <CardContent style={{ height:48, display: 'flex', alignItems: 'center'}}>
                  <Typography variant='title'>
                    {collection.title}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))
          : null
        }
      </Grid>
    );
  }
}

export default CollectionsList;
