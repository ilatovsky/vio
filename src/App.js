import Unsplash from './Unsplash';
import templates from './templates';
import config from './config';
const unsplash = new Unsplash({ clientId: config.clientId });

const render = ({ el, html }) => {
  document.getElementsByClassName(el)[0].innerHTML = html;
};


class App {
  constructor({ page, perPage, currentCollection }) {
    this.collectionsPage = page || 1;
    this.collectionsPerPage = perPage || 10;
    this.currentCollection = currentCollection || null;
    this.getCollections({});
  }

  listFromData = (data) => {
    render({ el: 'App__Sidebar', html: templates.collectionsList(data) });
    const links = document.getElementsByClassName('ListItem');
    Array.from(links).forEach(link => link.addEventListener('click', e => this.getCollection({ id: e.target.dataset.id })));
  };

  thumbsFromData = (data) => {
    render({ el: 'App__Content', html: templates.collectionPhotos(data) });
    const thumbs = document.getElementsByClassName('PhotoThumb');
    Array.from(thumbs).forEach(thumb => thumb.addEventListener('click', e => this.getPhoto({ id: e.path[1].dataset.id })));
  }

  getPhoto = ({id}) => {
    console.log(id);
  }

  getCollection = ({ id }) => {
    unsplash.getCollectionPhotos({ id })
      .then((data) => {
        console.log(data);
        this.thumbsFromData(data);
      });
  }

  getCollections = ({
    page: page = this.collectionsPage,
    perPage: perPage = this.collectionsPerPage,
  }) => {
    render({ el: 'App__Sidebar', html: templates.loading() });
    unsplash.getCollections({ page, perPage })
      .then((data) => {
        console.log(data);
        this.listFromData(data);
      });
  }
}

// unsplash.getCollectionsList({ perPage: 10 }).then(data => App.render({el:'App__Sidebar',list:data}));

export default new App(config);
